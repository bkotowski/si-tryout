require 'rubygems'

FactoryGirl.define do
  factory :car do
    price 185.0
    name 'Audi TT'
    people_number 5
    luggage_number 4
    doors_number 5
  end
end
