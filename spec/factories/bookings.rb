require 'rubygems'

FactoryGirl.define do
  factory :booking do
    pickup_location 1
    return_location 1
    pickup_datetime DateTime.now
    return_datetime DateTime.now
  end
end
