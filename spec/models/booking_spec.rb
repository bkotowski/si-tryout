require 'rails_helper'

describe Booking do

  let(:car) { FactoryGirl.create(:car) }
  let(:booking) { FactoryGirl.build(:booking, car: car)}

  it "has valid factory" do
    booking.should be_valid
  end
  
  describe 'pickup_datetime' do
    it "is invalid if not present" do
      booking.pickup_datetime = nil
      booking.should_not be_valid
    end
    it "is invalid if after return_datetime" do
      booking.pickup_datetime = booking.return_datetime + 10.days
      booking.should_not be_valid
    end
  end

  describe 'return_datetime' do
    it 'is invalid if not present' do
      booking.return_datetime = nil
      booking.should_not be_valid
    end
  end

  describe 'pickup_location' do
    it 'is invalid if not present' do
      booking.pickup_location = nil
      booking.should_not be_valid
    end
    it 'is invalid if out of locations array' do
      booking.pickup_location = 10
      booking.should_not be_valid
    end
  end

  describe 'return_location' do
    it 'is invalid if not present' do
      booking.return_location = nil
      booking.should_not be_valid
    end
    it 'is invalid if out of locations array' do
      booking.return_location = 10
      booking.should_not be_valid
    end
  end

  describe 'car' do
    it 'is invalid if not present' do
      booking.car = nil
      booking.should_not be_valid
    end
    it 'is invalid if not available' do
      booking.car.bookings.create(pickup_location: 1, return_location: 1, 
                                  pickup_datetime: booking.pickup_datetime, return_datetime: booking.return_datetime)
      booking.should_not be_valid
    end
  end
end
