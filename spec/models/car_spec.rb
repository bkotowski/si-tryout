require 'rails_helper'

describe Car do
  
  let(:booking) { FactoryGirl.build(:booking) }
  let(:car) { FactoryGirl.build(:car, bookings: [booking]) }

  it "has valid factory" do
    car.should be_valid
  end

  describe 'price' do
    it "is invalid if not present" do
      car.price = nil
      car.should_not be_valid
    end
    it "is invalid when negative" do
      car.price = -15
      car.should_not be_valid
    end
  end

  describe 'name' do
    it "is invalid if not present" do
      car.name = nil
      car.should_not be_valid
    end
  end

  describe 'people_number' do
    it "is invalid if not present" do
      car.people_number = nil
      car.should_not be_valid
    end
    it "is invalid when negative" do
      car.people_number = -15
      car.should_not be_valid
    end
    it "is invalid if not integer" do
      car.people_number = 3.3
      car.should_not be_valid
    end
  end

  describe 'doors_number' do
    it "is invalid if not present" do
      car.doors_number = nil
      car.should_not be_valid
    end
    it "is invalid when negative" do
      car.doors_number = -15
      car.should_not be_valid
    end
    it "is invalid if not integer" do
      car.doors_number = 3.3
      car.should_not be_valid
    end
  end

  describe 'luggage_number' do
    it "is invalid if not present" do
      car.luggage_number = nil
      car.should_not be_valid
    end
    it "is invalid when negative" do
      car.luggage_number = -15
      car.should_not be_valid
    end
    it "is invalid if not integer" do
      car.luggage_number = 3.3
      car.should_not be_valid
    end
  end
end
