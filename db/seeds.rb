# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
Car.create([
  { price: 170.0, name: "Audi A1", people_number: 4, luggage_number: 2, doors_number: 5, 
   photo: File.open(File.join(Rails.root, 'app', 'assets', 'images', 'audi_a1.jpg')) },
  { price: 185.0, name: "Seat Leon", people_number: 5, luggage_number: 4, doors_number: 5,
   photo: File.open(File.join(Rails.root, 'app', 'assets', 'images', 'seat_leon.jpg')) },
  { price: 190.0, name: "VW Touran", people_number: 5, luggage_number: 4, doors_number: 5,
   photo: File.open(File.join(Rails.root, 'app', 'assets', 'images', 'vw_touran.jpg')) }
])
