# README #

This is application developped during a tryout period in SI-Labs

## Ruby version ##
Application is using Ruby version 2.2.2

## Configuration ##
Application is keeping images on Amazon Web Services S3. To run it, you first need to
provide your AWS credentials and bucket name. To do it, you have to create a file config/local_env.yml with
**AWS_ACCESS_KEY_ID**, **AWS_SECRET_ACCESS_KEY**, **AWS_BUCKET_NAME** variables. You can also create
those variables in your local environment by running following command
  
```
#!sh

export VAR_NAME1=VAR_VALUE1 VAR_NAME2=VAR_VALUE2 ...
```

## Database configuration ##
MongoDB with Mongoid gem are used, so you must have the MongoDB instance running on your device.
You also want to setup your database connection in **config/mongoid.yml**

## Database initialization ##
To initialize database (seed it with cars) run 
  
```
#!sh

bundle exec rake db:seed
```


## How to run the test suite ##
Use following command from Rails root directory to run rspec tests

```
#!sh

bundle exec rspec spec
```