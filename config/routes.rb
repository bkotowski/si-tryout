Rails.application.routes.draw do

  resources :bookings, only: [:new, :create]
  resources :cars, only: [:show, :index]

  post '/cars' => 'cars#index'
  get '/bookings/:car_id/:booking_id' => 'bookings#show'

  root 'bookings#new'

end
