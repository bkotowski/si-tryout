class Booking
  include Mongoid::Document

  # Constant mapping of available pickup / return locations
  LOCATIONS = { 
    1 => 'Vienna central', 
    2 => 'Vienna airport',
    3 => 'Vienna station' 
  }

  # Fields
  field :pickup_datetime, type: DateTime
  field :return_datetime, type: DateTime
  field :pickup_location, type: Integer
  field :return_location, type: Integer

  # Relations
  embedded_in :car

  # Validations
  validates_presence_of :pickup_datetime, :return_datetime, :pickup_location, :return_location, :car
  validate :pickup_before_return_validation
  validate :car_is_available_validation
  %w(pickup return).each { |prefix| validate "#{prefix}_location_exists_validation".to_sym }

  # Custom getters / setters
  %w(pickup return).each do |prefix|                    # Get location na" }basing on id
    define_method "#{prefix}_location_name" do
      LOCATIONS[read_attribute("#{prefix}_location")]
    end
  end

  # Methods
  def total_price
    if car.present?
      (car.price / 3 * (return_datetime - pickup_datetime).to_i).to_i
    end
  end


  private
  def pickup_before_return_validation
    if pickup_datetime.present? && return_datetime.present? && pickup_datetime >= return_datetime
      errors.add(:pickup_datetime, 'should be before return_datetime')
    end
  end

  # Validation of pickup / return location
  %w(pickup return).each do |prefix|
    define_method "#{prefix}_location_exists_validation" do
      if read_attribute("#{prefix}_location").present? && LOCATIONS[read_attribute("#{prefix}_location")].nil?
        errors.add("#{prefix}_location".to_sym, "isn't available location")
      end
    end
  end

  def car_is_available_validation
    if car.present? && pickup_datetime.present? && return_datetime.present? &&
      !car.available_in?(pickup_datetime, return_datetime)
        errors.add(:car, "you chose isn't available")
      end
  end
end
