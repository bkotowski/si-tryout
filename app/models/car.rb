class Car
  include Mongoid::Document

  # Fields
  field :price, type: Float
  field :name, type: String
  field :people_number, type: Integer
  field :luggage_number, type: Integer
  field :doors_number, type: Integer

  # Relations
  embeds_many :bookings

  # Uploaders
  mount_uploader :photo, PhotoUploader

  # Validations
  validates_presence_of :price, :name, :people_number, :luggage_number, :doors_number
  validates_numericality_of :people_number, :luggage_number, :doors_number, greater_than: 0, only_integer: true
  validates_numericality_of :price, greater_than: 0

  # Methods
  def available_in? new_pickup, new_return
    bookings.or({:pickup_datetime.gt => new_return}, {:return_datetime.lt => new_pickup}).count == bookings.count
  end

  # Static methods
  def self.available_in new_pickup, new_return
    all.to_a.select { |c| c.available_in?(new_pickup, new_return) }
  end

end
