class CarsController < ApplicationController
  before_filter :set_booking

  def index
    pickup_dt = @booking.try(:pickup_datetime)
    return_dt = @booking.try(:return_datetime)
    @cars = Car.available_in(pickup_dt, return_dt)
  end

  def show
    @car = Car.find(params[:id])
    session[:car_id] = @car.id.to_s
  end

  private
  def booking_params
    params.require(:booking).permit(:pickup_datetime, :return_datetime, :pickup_location, :return_location)
  end

  def set_booking
    if params[:booking].present?
      session[:booking] = booking_params
    end
    @booking = Booking.new(session[:booking])
  end
end
