class BookingsController < ApplicationController
  def new
    render layout: false
  end

  def create
    @car = Car.find(params[:car_id])
    @booking = @car.bookings.build(session[:booking])
    redirect_to root_url if @booking.nil?
    if @booking.save
      flash[:success] = "You have chosen a #{@booking.car.name}"

      # Keeping it to know which booking is current
      session[:booking_id] = @booking.id.to_s
      session[:car_id] = @car.id.to_s

      redirect_to url_for(controller: :bookings, action: :show, car_id: @car.id.to_s, booking_id: @booking.id.to_s)
    end
  end

  def show
    @car = Car.find(params[:car_id])
    @booking = @car.bookings.find(params[:booking_id])
  end
end
