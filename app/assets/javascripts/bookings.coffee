class Window.NewBooking

  @monthNames = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']
  @return_location_selected = false
  @pickup_location_selected = false

  displayMap: ($object, latlng) ->
    $object.addClass('with-map')

    mapImgUrl = 'https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=200x200&center='+
      latlng.join(',')+'&markers=size:mid%7Ccolor:red%7C'+latlng.join(',')
    mapUrl = 'http://maps.google.com/?q=loc:'+latlng.join(',')

    $popover = $('<div/>').addClass('map-popover').append(
      $('<a/>').attr('href', mapUrl).attr('target', '_blank').append(
        $('<img/>').attr('src', mapImgUrl)
      )
    )
    $object.append($popover)

  slideBackground: ->
    $active = $('#car-slides .slide.active')
    $notActive = $('#car-slides .slide:not(.active)')
    $active.animate({left: '-100%'}, 1000, => $active.css('left', '100%'))
    $notActive.animate({left: '0'}, 1000)
    $active.removeClass('active')
    $notActive.addClass('active')


  constructor: ->
    # Datepicker
    pickup_picker = $("#booking_pickup_datetime")
    return_picker = $("#booking_return_datetime")
    $.timepicker.datetimeRange pickup_picker, return_picker,
      dateFormat: 'dd/mm/yy'
      timeText: 'uhrzeit'
      prevText: ''
      nextText: ''
      stepMinute: 10
      monthNames: Window.NewBooking.monthNames
      showButtonPanel: false
      oneLine: true
      controlType: 'select'
      minDateTime: new Date()


    # Selects
    $('select#booking_pickup_location').zelect
      placeholder: "Ort der Abholung"

    $('select#booking_return_location').zelect
      placeholder: "Ort der Rückgabe"

    $('#booking_pickup_location').on 'change', (e) => @pickup_location_selected = true
    $('#booking_return_location').on 'change', (e) => @return_location_selected = true

    # Map popover
    $('body').on 'mouseenter', '.zelect li', (e) =>
      if $(e.target).hasClass('with-map') or !$(e.target).is("li")
        return
      text = $(e.target).text()
      latlng = []
      if text.indexOf('station') isnt -1
        latlng = [48.186895,16.373736]
      else if text.indexOf('central') isnt -1
        latlng = [48.193414,16.366154]
      else if text.indexOf('airport') isnt -1
        latlng = [48.120903,16.563071]

      if latlng isnt []
        @displayMap($(e.target), latlng)

    $('body').on 'mouseleave', '.zelect li', (e) =>
      $obj = $(e.target)
      unless $obj.is('li')
        $obj = $obj.closest('li')
      if $obj.hasClass('with-map')
        $obj.children('.map-popover').remove()
        $obj.removeClass('with-map')

    $('body').on 'click', 'form input[type=submit]', (e) =>
      fields = ['booking_pickup_datetime', 'booking_return_datetime', 'booking_pickup_location', 'booking_return_location']

      valid = true
      $.each fields, (i, f) ->
        valid = false if $("##{f}").val() is "" or $("##{f}") is undefined
      valid = false unless @return_location_selected and @pickup_location_selected

      unless valid
        e.preventDefault()
        alert('You have to fill all fields')

    # Slides
    setInterval(@slideBackground, 15000)
