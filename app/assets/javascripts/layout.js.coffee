closeNotifications = ->
  $('.notification').fadeOut()

$(document).ready ->
  setTimeout(closeNotifications, 6000)
  $(window).on 'scroll', closeNotifications
