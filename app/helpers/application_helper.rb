module ApplicationHelper
  def current_booking_url
    if session[:car_id].present? && session[:booking_id].present?
      url_for(controller: :bookings, action: :show, car_id: session[:car_id], booking_id: session[:booking_id])
    else
      '#'
    end
  end
end
